# 什么是ArgoCD
Argo CD是一个基于Kubernetes的声明式的GitOps持续交付工具，使用Git库存储所需应用程序的配置。它的使用非常简单，并且自带一个简单实用的 Dashboard 页面，更重要的是 Argo CD 支持 kustomzie、helm、ksonnet 等多种工具。应用程序可以通过 Argo CD 提供的 CRD 资源对象进行配置，可以在指定的目标环境中自动部署所需的应用程序。
# 1、在线安装Argo CD
创建一个新的命名空间argocd，用于存放Argo CD服务和应用程序资源
```shell
kubectl create namespace argocd
git clone https://gitlab.com/snight/argocd.git
kubectl apply -n argocd -f argocd/install.yaml
```
查看argocd的pod资源
```shell
kubectl get pod,svc -n argocd
```
# 2、下载Argo CD CLI
下载Argo CD二进制文件到/usr/local/bin目录
```shell
wget --no-check-certificate https://github.com/argoproj/argo-cd/releases/download/v1.8.1/argocd-linux-amd64 -O argocd
mv argocd /usr/local/bin/argocd
```
赋予可执行权限
```shell
chmod +x /usr/local/bin/argocd
```
确认argo版本
```shell
argocd version
```
# 3、暴露web服务端口
将service的type类型改为NodePort，新增nodePort端口
```shell
kubectl edit -n argocd svc argocd-server
```

```yaml
...
spec:
  clusterIP: 172.16.29.41
  externalTrafficPolicy: Cluster
  ports:
  - name: http
    nodePort: 30080
    port: 80
    protocol: TCP
    targetPort: 8080
  - name: https
    nodePort: 30443
    port: 443
    protocol: TCP
    targetPort: 8080
  selector:
    app.kubernetes.io/name: argocd-server
  sessionAffinity: None
  type: NodePort
...
```

使用端口转发也可以用于连接到 API server，而无需暴露服务：
```
kubectl port-forward svc/argocd-server -n argocd 8080:443
```
然后使用 `localhost:8080` 访问 API server

# 4、访问 Argo Web
```
http://<K8S-NODE-IP>:30080
```
账号为admin，初始密码将自动生成为Argo CD API服务器的容器名称。可以使用以下命令查询：
```
kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2
```
# 5、修改密码
使用用户名 admin 和上面的密码，登录到 Argo CD 的 IP 或主机名：
```
argocd login <ARGOCD_SERVER_IP>
```
使用以下命令修改密码：
```
argocd account update-password
```

```shell
# 登录
$ argocd login localhost:8080
WARNING: server certificate had error: x509: certificate signed by unknown authority. Proceed insecurely (y/n)? yes
Username: admin
Password:
'admin' logged in successfully
Context 'localhost:8080' updated

# 修改密码
$ argocd account update-password
*** Enter current password:
*** Enter new password:
*** Confirm new password:
Password updated
Context 'localhost:8080' updated
```
如果忘记密码，可使用下面的命令重置为123456
```shell
kubectl -n argocd patch secret argocd-secret -p '{"stringData": { "admin.password":"$2a$10$rRyBsGSHK6.uc8fntPwVIuLVHgsAhAX7TcdrqW/RADU0uh7CaChLa","admin.passwordMtime": "'$(date +%FT%T%Z)'"}}'
```
6、注册集群以便部署应用程序（可选）
此操作会将集群的凭据注册到 Argo CD，仅当部署到外部集群时才需要。在内部进行部署（与 Argo CD 运行在同一集群）时，应将https://kubernetes.default.svc 用作应用程序的 K8s API server 地址。

列出当前 kubeconfig 中的所有集群列表：
```shell
# 当前配置的集群列表
argocd cluster add
# 注册集群
argocd cluster add kubernetes-admin@kubernetes
```
执行上面的命令会将 ServiceAccount（argocd-manager）安装到该 kubectl 的 kube-system 命名空间中，并将该服务帐户绑定到管理员级别的 ClusterRole。 Argo CD 使用此服务帐户令牌执行其管理任务（即部署/监视）。
